SET NEWPATH=%CD%\..\clrreadlocal.bat
%NEWPATH% %1

REM attempts to execute the batch file in the b4a project, one level above the objects folder which is the main project folder.
REM This file would not be necessary if the Main batch file could be called from CD directory as in the path on the first line
REM As it is B4A custom command won't find the file in the path or project, unless the path is explicit.

